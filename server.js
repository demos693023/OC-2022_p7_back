//========//IMPORTS
const http = require('http');
const https = require('https');
const app = require('./app');
const fs = require('fs')
const path = require('path');

//========//CONFIG

const server = http.createServer(app);

const sslServer = https.createServer({
    key: fs.readFileSync(path.join(__dirname, 'cert', 'key.pem')),
    cert: fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'))
}, app);

function startMessage (protocol, port) {
    console.log(protocol + " server run on port " + port)
}

//========//CREATE SERVERS
const httpPort = process.env.PORT_HTTP ? Number(process.env.PORT_HTTP) : 80
const sslPort = process.env.PORT_HTTPS ? Number(process.env.PORT_HTTPS) : 443

server.listen(httpPort, startMessage('HTTP', httpPort)); 
sslServer.listen(sslPort, startMessage('HTTPS', sslPort));